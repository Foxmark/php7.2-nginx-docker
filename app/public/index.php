<?php
$redis = new Redis();
try {
    if($redis->connect(getenv('REDIS_HOST'), getenv('REDIS_PORT'))) {
        echo 'Connected to redis ' . getenv('REDIS_HOST') . '<br>';
    }
} catch (Exception $e) {
    echo 'Unable to connect to redis {host}' . '<br>';
}

echo '------>' . getenv('MYSQL_HOST', true) . '<br>';
echo '------>' . getenv('MYSQL_DB', true) . '<br>';
echo '------>' . getenv('MYSQL_PASS', true) . '<br>';
echo '------>' . getenv('MYSQL_USER', true) . '<br>';
echo '------>' . getenv('MYSQL_PORT', true) . '<br>';
echo '------>' . getenv('VAR_B', true) . '<br>';
echo '<hr>';
phpinfo();
